package ictgradschool.industry.lab08.ex04;

import ictgradschool.industry.lab08.ex03.Movie;
import ictgradschool.industry.lab08.ex03.MovieReader;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.Scanner;

/**
 * Created by anhyd on 20/03/2017.
 */
public class Ex4MovieReader extends MovieReader {

    @Override
    protected Movie[] loadMovies(String fileName) {

        // TODO Implement this with a PrintWriter
        File input = new File(fileName);
        int counter=0;
        try(Scanner input_film = new Scanner(new FileReader(input))){
            while (input_film.hasNextLine()){
                input_film.nextLine();
                counter++;
            }
        }
        catch (Exception e){
            System.out.println("Error: "+ e.getMessage());
        }
        int i=0;
        Movie[] movie_list = new Movie[counter];
        try(Scanner input_film = new Scanner(new FileReader(input))){
            input_film.useDelimiter(",|\\r\\n");
        while(input_film.hasNext()) {
            String movie_name = input_film.next();
            int movie_year = input_film.nextInt();
            int movie_length = input_film.nextInt();
            String movie_director = input_film.next();
            movie_list[i++] = new Movie(movie_name,movie_year,movie_length,movie_director);
            }

        }
        catch (IOException e){
            System.out.println("Error: "+ e.getMessage());
        }
        return movie_list;
    }

    public static void main(String[] args) {
        new Ex4MovieReader().start();
    }
}
