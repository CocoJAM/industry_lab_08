package ictgradschool.industry.lab08.ex04;

import ictgradschool.industry.lab08.ex03.Movie;
import ictgradschool.industry.lab08.ex03.MovieWriter;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;

/**
 * Created by anhyd on 20/03/2017.
 */
public class Ex4MovieWriter extends MovieWriter {

    @Override
    protected void saveMovies(String fileName, Movie[] films) {

        // TODO Implement this with a Scanner
        File output= new File(fileName);
        try(PrintWriter output_writer = new PrintWriter(new FileWriter(output))){
            for (int i = 0; i < films.length; i++) {
                Movie film = (Movie) films[i];
                output_writer.print(film.getName()+",");
                output_writer.print(film.getYear()+",");
                output_writer.print(film.getLengthInMinutes()+",");
                output_writer.println(film.getDirector());
            }
        }
        catch (IOException e){
            System.out.println("Error: " + e.getMessage());
        }
    }

    public static void main(String[] args) {
        new Ex4MovieWriter().start();
    }

}
