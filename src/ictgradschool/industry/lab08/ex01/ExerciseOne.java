package ictgradschool.industry.lab08.ex01;
import java.io.*;

public class ExerciseOne {

    public void start() {

        printNumEsWithFileReader();

        printNumEsWithBufferedReader();

    }

    private void printNumEsWithFileReader() {

        int numE = 0;
        int total = 0;

        // TODO Read input2.txt and print the total number of characters, and the number of e and E characters.
        // TODO Use a FileReader.
        File a = new File("input2.txt");
        try(FileReader input_a= new FileReader(a)){
            int sentence = 0;
            char Es;
        while((sentence = input_a.read()) != -1){
            total++;
            if (sentence == (int) 'e'|| sentence == (int) 'E'){
                numE++;
            }
        }

            System.out.println("Number of e/E's: " + numE + " out of " + total);
        }
        catch (IOException e){
        System.out.println("Error: " + e.getMessage());
        }




    }

    private void printNumEsWithBufferedReader() {

        int numE = 0;
        int total = 0;

        // TODO Read input2.txt and print the total number of characters, and the number of e and E characters.
        // TODO Use a BufferedReader.
        File a = new File("input2.txt");
        String sentence = null;
        try(BufferedReader input_a = new BufferedReader(new FileReader(a))){
        while((sentence =input_a.readLine()) != null){
            total += sentence.length();
            for (int i = 0; i < sentence.length(); i++) {
                if(sentence.charAt(i)=='e'||sentence.charAt(i)=='E'){
                    numE++;
                }
            }
        }
            System.out.println("Number of e/E's: " + numE + " out of " + total);
        }
        catch (IOException e){
            System.out.println("Error: " + e.getMessage());
        }
    }

    public static void main(String[] args) {
        new ExerciseOne().start();
    }

}
