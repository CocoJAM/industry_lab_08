package ictgradschool.industry.lab08.ex02;

import ictgradschool.Keyboard;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.Scanner;

public class MyScanner {

    public void start() {

        // TODO Prompt the user for a file name, then read and print out all the text in that file.
        // TODO Use a Scanner.
        File input = new File(Keyboard.readInput());
        try (Scanner reader = new Scanner(new FileReader(input))){
            while (reader.hasNext()){
                System.out.println(reader.next());
            }
        }
        catch(IOException e){
            System.out.println("Error: "+e.getMessage());
        }
    }

    public static void main(String[] args) {
        new MyScanner().start();
    }
}
