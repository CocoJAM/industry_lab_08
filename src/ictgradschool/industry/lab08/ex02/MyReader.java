package ictgradschool.industry.lab08.ex02;

import ictgradschool.Keyboard;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.nio.Buffer;

public class MyReader {

    public void start() {

        // TODO Prompt the user for a file name, then read and print out all the text in that file.
        // TODO Use a BufferedReader.
        String user_input_file = Keyboard.readInput();
        File a = new File(user_input_file);
        String line = null;
        try(BufferedReader reader = new BufferedReader(new FileReader(user_input_file))){
            while ((line = reader.readLine()) != null){
                System.out.println(line);
            }
        }
        catch(IOException e){
            System.out.println("Error: "+ e.getMessage());
        }

    }

    public static void main(String[] args) {
        new MyReader().start();
    }
}
