package ictgradschool.industry.lab08.ex02;

import ictgradschool.Keyboard;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;

/**
 * A simple program which should allow the user to type any number of text lines. The program will then
 * write them out to a file.
 */
public class MyWriter {

    public void start() {

        System.out.print("Enter a file name: ");
        String fileName = Keyboard.readInput();

        // TODO Open a file for writing, using a PrintWriter.
        File a = new File(fileName);
        String words = "";
        while (true) {
            System.out.print("Type a line of text, or just press ENTER to quit: ");
           try(PrintWriter writer = new PrintWriter(new FileWriter(a))){
               String text = Keyboard.readInput();
               words += text+ "\n";
               if (text.isEmpty()) {
                   writer.print(words);
                   break;
               }
               System.out.println("Done!");
           }
           catch (IOException e){
               System.out.println("Error: "+ e.getMessage());
           }

            // TODO Write the user's line of text to a file.
        }



    }

    public static void main(String[] args) {

        new MyWriter().start();

    }
}
